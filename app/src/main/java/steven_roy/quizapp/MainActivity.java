package steven_roy.quizapp;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    int a, b, c, d, e, f, g, h, i, j, k, l;


    public void Submit(View v) {
        TextView hasil = (TextView) findViewById(R.id.hasil);
        EditText tanya = (EditText) findViewById(R.id.Book);
        String str = tanya.getText().toString();

        RadioButton footballer = (RadioButton) findViewById(R.id.jawabanA);
        RadioButton basketball = (RadioButton) findViewById(R.id.jawabanB);
        RadioButton tennis = (RadioButton) findViewById(R.id.jawabanC);
        RadioButton singer = (RadioButton) findViewById(R.id.jawabanD);
        RadioButton england = (RadioButton) findViewById(R.id.jawabanE);
        RadioButton italia = (RadioButton) findViewById(R.id.jawabanF);
        RadioButton portugal = (RadioButton) findViewById(R.id.jawabanG);
        RadioButton timor = (RadioButton) findViewById(R.id.jawabanH);
        RadioButton presiden = (RadioButton) findViewById(R.id.jawabanM);
        RadioButton presiden1 = (RadioButton) findViewById(R.id.jawabanN);
        RadioButton presiden2 = (RadioButton) findViewById(R.id.jawabanO);
        RadioButton presiden3 = (RadioButton) findViewById(R.id.jawabanP);

        CheckBox bill = (CheckBox) findViewById(R.id.cbA);
        CheckBox larry = (CheckBox) findViewById(R.id.cbB);
        CheckBox steve = (CheckBox) findViewById(R.id.cbC);
        CheckBox sergey = (CheckBox) findViewById(R.id.cbD);


        try {
            if (footballer.isChecked()) {
                a = 20;
            } else if (basketball.isChecked() || tennis.isChecked()
                    || singer.isChecked()) {
                b = 0;
            }
            if (portugal.isChecked()) {
                c = 20;
            } else if (england.isChecked() || italia.isChecked()
                    || timor.isChecked()) {
                d = 0;
            }
            if (steve.isChecked() || bill.isChecked() && !larry.isChecked() && !sergey.isChecked()) {
                e = 0;
            } else if (larry.isChecked() && sergey.isChecked()) {
                f = 20;
            } else {
                g = 0;
            }

            if (str.contains("buku") || str.contains("BUKU")
                    || str.contains("Buku")) {
                h = 20;
            } else {
                i = 0;
            }
            if (presiden.isChecked()) {
                j = 20;
            } else if (presiden1.isChecked() || presiden2.isChecked()
                    || presiden3.isChecked()) {
                k = 0;
            }
            l = a + b + c + d + e + f + g + h + i + j + k;

            if (l > 60) {
                Toast toast = Toast.makeText(this, "You're passed", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                hasil.setTextColor(Color.parseColor("#4caf50"));
                hasil.setText("Nilai Anda: " + l + "\nSelamat Anda LULUS");
            } else {
                Toast toast = Toast.makeText(this, "You're not passed", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                hasil.setTextColor(Color.parseColor("#d32f2f"));
                hasil.setText("Nilai Anda: " + l + "\nAnda tidak LULUS");
            }
        } catch (Exception e) {
            final Toast toast = Toast.makeText(getApplicationContext(), "This message will disappear in half a second", Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 500);
        }
    }
}

